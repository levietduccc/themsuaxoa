//
//  CarDetailViewController.swift
//  ThemSuaXoa
//
//  Created by Le Viet Duc on 07/01/2019.
//  Copyright © 2019 Le Viet Duc. All rights reserved.
//

import UIKit

class CarDetailViewController: UIViewController {
    
    var name: String?
    

    @IBOutlet weak var carName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        if name != nil {
            carName.text = name
        }

        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

            name = carName.text
    }
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
